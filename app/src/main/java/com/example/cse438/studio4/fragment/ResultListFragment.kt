package com.example.cse438.studio4.fragment

import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.util.DiffUtil
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.cse438.studio4.R
import com.example.cse438.studio4.activity.ProductDetailActivity
import com.example.cse438.studio4.model.Product
import com.example.cse438.studio4.viewmodel.ProductViewModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_result_list.*
import kotlinx.android.synthetic.main.result_list_item.view.*
import java.text.NumberFormat


@SuppressLint("ValidFragment")
class ResultListFragment(context: Context, query: String): Fragment() {
    private var adapter = ResultAdapter()
    private var parentContext: Context = context
    private lateinit var viewModel: ProductViewModel
    private var listInitialized = false

    private var queryString: String = query
    private var productList: ArrayList<Product> = ArrayList()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_result_list, container, false)
    }

    override fun onStart() {
        super.onStart()
        val displayText = "Search for: $queryString"
        (activity as AppCompatActivity).supportActionBar?.title = displayText

        result_items_list.layoutManager = LinearLayoutManager(parentContext)
        result_items_list.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        viewModel = ViewModelProviders.of(this).get(ProductViewModel::class.java)

        val observer = Observer<ArrayList<Product>> {
            result_items_list.adapter = adapter
            val result = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
                override fun areItemsTheSame(p0: Int, p1: Int): Boolean {
                    return productList[p0].getId() == productList[p1].getId()
                }

                override fun getOldListSize(): Int {
                    return productList.size
                }

                override fun getNewListSize(): Int {
                    if (it == null) {
                        return 0
                    }
                    return it.size
                }

                override fun areContentsTheSame(p0: Int, p1: Int): Boolean {
                    return (productList[p0] == productList[p1])
                }
            })
            result.dispatchUpdatesTo(adapter)
            productList = it ?: ArrayList()
        }

        viewModel.getProductsByQueryText(queryString).observe(this, observer)

        this.listInitialized = true
    }

    inner class ResultAdapter: RecyclerView.Adapter<ResultAdapter.ResultViewHolder>() {

        override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ResultViewHolder {
            val itemView = LayoutInflater.from(p0.context).inflate(R.layout.result_list_item, p0, false)
            return ResultViewHolder(itemView)
        }

        override fun onBindViewHolder(p0: ResultViewHolder, p1: Int) {
            val product = productList[p1]
            val productImages = product.getImages()
            if (productImages.size == 0) {

            }
            else {
                Picasso.with(this@ResultListFragment.context).load(productImages[0]).into(p0.productImg)
            }
            p0.productTitle.text = product.getProductName()

            val price = product.getPrice()
            if (price == -1.0) {
                p0.productPrice.text = getString(R.string.no_pricing)
            }
            else {
                val priceString = NumberFormat.getCurrencyInstance().format(price)
                p0.productPrice.text = priceString
            }

            p0.row.setOnClickListener {
                val intent = Intent(this@ResultListFragment.parentContext, ProductDetailActivity::class.java)
                intent.putExtra("PRODUCT", product)
                startActivity(intent)
            }

//            if (this@ResultListFragment.listInitialized) {
//                if (product.isFavorite) {
//                    p0.productFavorite.setImageResource(R.drawable.ic_star_24dp)
//                }
//            }
//
//            p0.productFavorite.setOnClickListener {
//                product = productList[p1]
//                if (product.isFavorite) {
//                    this@ResultListFragment.viewModel.removeFavorite(product.getId(), true)
//                    p0.productFavorite.setImageResource(R.drawable.ic_star_border_24dp)
//                }
//                else {
//                    this@ResultListFragment.viewModel.addFavorite(product)
//                    p0.productFavorite.setImageResource(R.drawable.ic_star_24dp)
//                }
//                productList[p1].isFavorite = !product.isFavorite
//            }
        }

        override fun getItemCount(): Int {
            return productList.size
        }

        inner class ResultViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
            val row = itemView

            var productImg: ImageView = itemView.product_img
            var productTitle: TextView = itemView.product_title
            var productPrice: TextView = itemView.product_price
            //var productFavorite: ImageView = itemView.favorite_icon
        }
    }
}