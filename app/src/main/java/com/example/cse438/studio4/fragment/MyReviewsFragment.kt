package com.example.cse438.studio4.fragment

import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.util.DiffUtil
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.cse438.studio4.R
import com.example.cse438.studio4.activity.ProductDetailActivity
import com.example.cse438.studio4.model.Product
import com.example.cse438.studio4.viewmodel.ProductViewModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_my_reviews.*
import kotlinx.android.synthetic.main.review_list_item.view.*
import java.text.NumberFormat

@SuppressLint("ValidFragment")
class MyReviewsFragment(context: Context): Fragment() {
    private var adapter = ReviewAdapter()
    private var parentContext: Context = context
    private lateinit var viewModel: ProductViewModel

    private var productList: ArrayList<Product> = ArrayList()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return layoutInflater.inflate(R.layout.fragment_my_reviews, container, false)
    }

    override fun onStart() {
        super.onStart()

        review_items_list.layoutManager = LinearLayoutManager(parentContext)
        review_items_list.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        viewModel = ViewModelProviders.of(this).get(ProductViewModel::class.java)

        val observer = Observer<ArrayList<Product>> {
            review_items_list.adapter = adapter
            val result = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
                override fun areItemsTheSame(p0: Int, p1: Int): Boolean {
                    return productList[p0].getId() == productList[p1].getId()
                }

                override fun getOldListSize(): Int {
                    return productList.size
                }

                override fun getNewListSize(): Int {
                    if (it == null) {
                        return 0
                    }
                    return it.size
                }

                override fun areContentsTheSame(p0: Int, p1: Int): Boolean {
                    return (productList[p0] == productList[p1])
                }
            })
            result.dispatchUpdatesTo(adapter)
            productList = it ?: ArrayList()
        }

        viewModel.getReviewedProducts().observe(this, observer)
    }

    inner class ReviewAdapter: RecyclerView.Adapter<ReviewAdapter.ReviewViewHolder>() {
        override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ReviewAdapter.ReviewViewHolder {
            val itemView = LayoutInflater.from(p0.context).inflate(R.layout.review_list_item, p0, false)
            return ReviewViewHolder(itemView)
        }

        override fun onBindViewHolder(p0: ReviewAdapter.ReviewViewHolder, p1: Int) {
            val product = productList[p1]
            val productImages = product.getImages()
            if (productImages.size == 0) {

            }
            else {
                Picasso.with(this@MyReviewsFragment.parentContext).load(productImages[0]).into(p0.productImg)
            }
            p0.productTitle.text = product.getProductName()

            val price = product.getPrice()
            if (price == -1.0) {
                p0.productPrice.text = getString(R.string.no_pricing)
            }
            else {
                val priceString = NumberFormat.getCurrencyInstance().format(price)
                p0.productPrice.text = priceString
            }

            p0.row.setOnClickListener {
                val intent = Intent(this@MyReviewsFragment.parentContext, ProductDetailActivity::class.java)
                intent.putExtra("PRODUCT", product)
                startActivity(intent)
            }

//            if (this@ResultListFragment.listInitialized) {
//                if (product.isFavorite) {
//                    p0.productFavorite.setImageResource(R.drawable.ic_star_24dp)
//                }
//            }
//
//            p0.productFavorite.setOnClickListener {
//                product = productList[p1]
//                if (product.isFavorite) {
//                    this@ResultListFragment.viewModel.removeFavorite(product.getId(), true)
//                    p0.productFavorite.setImageResource(R.drawable.ic_star_border_24dp)
//                }
//                else {
//                    this@ResultListFragment.viewModel.addFavorite(product)
//                    p0.productFavorite.setImageResource(R.drawable.ic_star_24dp)
//                }
//                productList[p1].isFavorite = !product.isFavorite
//            }
        }

        override fun getItemCount(): Int {
            return productList.size
        }

        inner class ReviewViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
            val row = itemView

            var productImg: ImageView = itemView.product_img
            var productTitle: TextView = itemView.product_title
            var productPrice: TextView = itemView.product_price
        }
    }
}